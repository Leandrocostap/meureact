import React from 'react'
import styles from './Article.module.css'

const Article = ({ index, titleArticle, articleContent, artigos, setArtigos }) => {
  return (
    <div key={index} className={styles.artigoContainer}>
      <div className={styles.artigoContent}>
        <h2>{titleArticle}</h2>
        <p>{articleContent}</p>
      </div>
      <div className={styles.lixeira}>
        <button
          onClick={() => {
            const newArray = [...artigos]
            newArray.splice(index, 1)
            setArtigos(newArray)
          }}
        >
          <img src="lixeira.svg" alt="Lixeira" />
        </button>
      </div>
    </div>
  )
}

export default Article